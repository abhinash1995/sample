package com.example.mapstruct;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DetailsRepositary extends JpaRepository<Details, Long> {

}
