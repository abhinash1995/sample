package com.example.mapstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DetailsService {

    @Autowired
    DetailsRepositary detailsRepositary;

//to save the data
    public DetailsModel save(DetailsModel d){
        Details d1=DetailsMapper.INSTANCE.detailsModeltoDetails(d);
      detailsRepositary.save(d1);
      return d;

    }
//search all employee
    public List<Details> findAll(){
        return detailsRepositary.findAll();
    }
    //get employee by id
    public Details findOne(long id){
        return detailsRepositary.getOne(id);
    }

////update
//    public void updateDetails(String id,Details d){
//        detailsRepositary.save(d);
//    }



    //delete
//    public void delete(Details d1){
//        detailsRepositary.delete(d1);
//    }
}

