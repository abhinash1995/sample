package com.example.mapstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value="/details")
public class DetailsController {

  @Autowired
    DetailsService detailsService;

    @GetMapping("/hello")
    public String index() {
        return "Greetings from Spring Boot!";
    }

//to save
    @PostMapping("/Details")
    public DetailsModel createDetails(@Valid @RequestBody DetailsModel detailsmodel){
        return  detailsService.save(detailsmodel);
    }
    //  get all details
    @GetMapping("/Details")
    public List<Details> getAllDetails() {
        return detailsService.findAll();
    }
////to update
//    @PutMapping("/Details/{id}")
//    public ResponseEntity<Details> updateDetails(@PathVariable(value = "id") long id, @Valid @RequestBody Details d){
//        Details det=detailsService.findOne(id);
//        if(det==null) {
//            return ResponseEntity.notFound().build();
//        }
//        det.setName(d.getName());
//        det.setImage(d.getImage());
//        det.setDescription(d.getDescription());
//        Details updateDetails=detailsService.save(det);
//        return ResponseEntity.ok().body(updateDetails);
//    }


//get details by id
@GetMapping(value = "/Details/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
public ResponseEntity<Details> getDetailsById(@PathVariable(value = "id") long id){
    Details det=detailsService.findOne(id);
    if(det==null) {
        return ResponseEntity.notFound().build();
    }
      Object o1= ResponseEntity.ok().body(det);
    return ResponseEntity.ok(det);
}
//delete
//    @DeleteMapping("/Details/{id}")
//    public ResponseEntity<Details> deleteDetails(@PathVariable(value = "id") long id){
//        Details det=detailsService.findOne(id);
//        if(det==null) {
//            return ResponseEntity.notFound().build();
//        }
//        detailsService.delete(det);
//
//        return ResponseEntity.ok().build();
//    }

}
