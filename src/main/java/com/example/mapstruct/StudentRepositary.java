package com.example.mapstruct;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface StudentRepositary extends JpaRepository<StudentEntity, Long> {

    @Query("SELECT s FROM StudentEntity s WHERE s.sex = 'M'")
    List<StudentEntity> getBySex();


    //or   @Query("SELECT s FROM StudentEntity s WHERE s.percentage > ?1")
    @Query("SELECT s FROM StudentEntity s WHERE s.percentage >?1")//JPQL query
    List<StudentEntity> getByPercentage(@Param("percentage") Double percentage);

    @Query(value = "SELECT * FROM student WHERE yop = :yop", nativeQuery = true)
    List<StudentEntity> findByYop(@Param("yop") int yop);
}
