package com.example.mapstruct;

import com.fasterxml.jackson.databind.node.ObjectNode;

public class StudentModel {

    private long id;

    private String name;

    private char sex;

    private String email;

    private String branch;

    private Double percentage;

    private int yop;

    private ObjectNode address;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public char getSex() {
        return sex;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public Double getPercentage() {
        return percentage;
    }

    public void setPercentage(Double percentage) {
        this.percentage = percentage;
    }

    public int getYop() {
        return yop;
    }

    public void setYop(int yop) {
        this.yop = yop;
    }

    public ObjectNode getAddress() {
        return address;
    }

    public void setAddress(ObjectNode address) {
        this.address = address;
    }
}
