package com.example.mapstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value="/details")
public class StudentController {

    @Autowired
    StudentService studentService;

    @GetMapping("/student")
    public String index() {
        return "Greetings from Spring Boot!";
    }

    //to save
    @PostMapping("/student_details")
    public StudentModel saveDetails(@Valid @RequestBody StudentModel studentModel){
        return studentService.save(studentModel);
    }

    @GetMapping("/getDetailsbyGender")
    public List<StudentModel> getByGender(){
        return studentService.getBySex();
    }

    @GetMapping("/getDetailsbyPercentage/{percentage}")
    public List<StudentModel> getByPercentage(@PathVariable(value = "percentage") Double percentage){
        return studentService.getByPerc(percentage);
    }

    @GetMapping("/get_details_by_yop/{yop}")
    public List<StudentModel> getByYearOfPassout(@PathVariable(value = "yop") int yop){
        return studentService.getByYop(yop);
    }



}
