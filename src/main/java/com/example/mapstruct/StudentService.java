package com.example.mapstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService {

   @Autowired
    StudentRepositary studentRepositary;

    public StudentModel save(StudentModel studentModel){
        StudentEntity studentEntity= StudentMapper.INSTANCE.modelToStudentEntity(studentModel);
//        studentRepositary.save(studentEntity);
        return StudentMapper.INSTANCE.entityToStudentModel(studentRepositary.save(studentEntity));
    }

    public List<StudentModel> getBySex(){

        return StudentMapper.INSTANCE.listEntityToStudentModel(studentRepositary.getBySex());
    }

    public List<StudentModel> getByPerc(Double per){
        return StudentMapper.INSTANCE.listEntityToStudentModel(studentRepositary.getByPercentage(per));
    }

    public List<StudentModel> getByYop(int yop){
        return StudentMapper.INSTANCE.listEntityToStudentModel(studentRepositary.findByYop(yop));
    }



}
