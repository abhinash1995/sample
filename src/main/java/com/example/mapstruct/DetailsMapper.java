package com.example.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper
public interface DetailsMapper {
    static DetailsMapper INSTANCE = Mappers.getMapper( DetailsMapper.class );

    DetailsModel detailsToDetailsModel(Details details);
    Details detailsModeltoDetails(DetailsModel detailsmodel);


//    Details DetailsDtotoDetails(DetailsModel detailsDto);
}
