package com.example.mapstruct;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.vladmihalcea.hibernate.type.array.IntArrayType;
import com.vladmihalcea.hibernate.type.array.StringArrayType;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "states")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@TypeDefs({
        @TypeDef(
                name = "string-array",
                typeClass = StringArrayType.class
        ),
        @TypeDef(
                name = "int-array",
                typeClass = IntArrayType.class
        ),
        @TypeDef(name = "json", typeClass = JsonBinaryType.class)
})
public class Details {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private long id;

    @Column(name = "name")
   // @JsonProperty("n") we can replace name by n using json property
    private String name;

    @Column(name = "image")
    private String image;

    @Column(name = "description")
    private String description;

    @Type( type = "string-array" )
    @Column(
            name = "tags",
            columnDefinition = "text[]"
    )
    private String[] tags;

    @Type( type = "int-array" )
    @Column(
            name = "rating",//if we give different nAME IT WILL create that col in table and store there
            columnDefinition = "integer[]"
    )
    private int[] rating;

    @Type( type = "json" )
    @Column( columnDefinition = "json" )
//    @JsonProperty("cities")  //if we use this the it will map to city col in db irrespective of name declared in java
    private ArrayNode cities;

    @Type( type = "json" )
    @Column( columnDefinition = "json" )
    private ObjectNode deatil;

    public ObjectNode getDeatil() {
        return deatil;
    }

    public void setDeatil(ObjectNode deatil) {
        this.deatil = deatil;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public String[] getTags() {
        return tags;
    }
    public void setTags(String[] tags) {
        this.tags = tags;
    }

    public int[] getRating() {
        return rating;
    }
    public void setRating(int[] rating) {
       this.rating = rating;
    }

    public ArrayNode getCities() {
        return cities;
    }

    public void setCities(ArrayNode cities) {
        this.cities = cities;
    }
}
