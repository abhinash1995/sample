package com.example.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface StudentMapper {

    StudentMapper INSTANCE = Mappers.getMapper( StudentMapper.class );

    StudentEntity modelToStudentEntity(StudentModel studentModel);

    StudentModel entityToStudentModel(StudentEntity studentEntity);

    List<StudentModel> listEntityToStudentModel(List<StudentEntity> studentEntity);
}
